using DAL.DTO;
namespace DAL.Repositories;


public class FundHoldingsTickerRepositoryCsv: IFundHoldingsTickerReadonlyRepository
{
    private readonly string _folderPath;
    private List<FundHoldingsTickerDTO> _fundHoldingsTickers = new();
    
    public FundHoldingsTickerRepositoryCsv(string folderPath)
    {
        _folderPath = folderPath;
        LoadTickerData();
    }
    
    private void LoadTickerData()
    {
            foreach (var file in Directory.EnumerateFiles(_folderPath, "*.csv"))
            {
                try
                {
                    foreach (var ticker in TickerDataCsvLoader.LoadTickerDataFromCsv(file))
                    {
                        _fundHoldingsTickers.Add(ticker);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception($"Failed to load ticker data from {_folderPath}", ex);
                }
            }
    }
    
    public Dictionary<string, FundHoldingsTickerDTO> GetFundHoldingsTickersForDate(DateTime date)
    {
        return _fundHoldingsTickers.Where(t => t.Date == date.Date).ToDictionary(t => t.Ticker);
    }

    public IEnumerable<FundHoldingsTickerDTO> GetFundHoldingsTickerHistory(string ticker)
    {
        return _fundHoldingsTickers.Where(t => t.Ticker == ticker).OrderBy(t => t.Date);
    }
}
