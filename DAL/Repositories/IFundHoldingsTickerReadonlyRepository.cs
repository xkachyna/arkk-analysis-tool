using DAL.DTO;

namespace DAL.Repositories;

/// <summary>
/// Allows querying and adding FundHoldingsTickerDTO records to a repository
/// for comparing and analyzing tickers.
/// </summary>
public interface IFundHoldingsTickerReadonlyRepository
{
    /// <summary>
    /// Gets a dictionary of tickers and their FundHoldingsTickerDTO for a given date
    /// </summary>
    /// <param name="date">Date for which to get ticker values for (only the date part is used)</param>
    /// <returns>Dictionary of tickers and their FundHoldingsTickerDTO for a given date</returns>
    public Dictionary<string, FundHoldingsTickerDTO> GetFundHoldingsTickersForDate(DateTime date);
    
    /// <summary>
    /// Gets a list of FundHoldingsTickerDTO for a given ticker (such as "AAPL") sorted by date
    /// </summary>
    /// <param name="ticker"></param>
    /// <returns></returns>
    public IEnumerable<FundHoldingsTickerDTO> GetFundHoldingsTickerHistory(string ticker);
}
