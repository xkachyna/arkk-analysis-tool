using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using DAL.DTO;

namespace DAL;

/// <summary>
/// Utility for loading ticker data from CSV files
/// </summary>
public static class TickerDataCsvLoader
{
    private static readonly CultureInfo CultureInfo;
    private static readonly CsvConfiguration Config;
    
    static TickerDataCsvLoader()
    {
        CultureInfo = new CultureInfo("en-US");
        Config = new CsvConfiguration(CultureInfo.InvariantCulture) {
             HasHeaderRecord = true,
             MissingFieldFound = null,
         };
        Config.MissingFieldFound = args => throw new Exception($"Missing field at row {args.Index}");
    }
    
    /// <summary>
    /// Yields ticker data from a CSV file
    /// </summary>
    /// <param name="filePath">File to load ticker data from</param>
    /// <returns></returns>
    public static IEnumerable<FundHoldingsTickerDTO> LoadTickerDataFromCsv(string filePath)
    {
        using var reader = new StreamReader(filePath);
        using var csv = new CsvReader(reader, Config);

        foreach (var record in csv.GetRecords<TickerDataCsvRecord>())
        {
            yield return new FundHoldingsTickerDTO
            {
                Date = record.Date,
                Company = record.Company,
                Ticker = record.Ticker,
                MarketValueUsd = ParseMarketValue(record.MarketValueUsd),
                WeightPercentage = ParseWeightPercentage(record.WeightPercentage)
            };
        }
    }
    
    private static decimal ParseMarketValue(string marketValue)
    {
        marketValue = marketValue.Replace(",", "");
        marketValue = marketValue.Replace("$", "");
        return decimal.Parse(marketValue, CultureInfo);
    }
    
    private static decimal ParseWeightPercentage(string weightPercentage)
    {
        weightPercentage = weightPercentage.Replace("%", "");
        return decimal.Parse(weightPercentage, CultureInfo);
    }
}
