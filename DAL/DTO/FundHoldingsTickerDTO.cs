namespace DAL.DTO;

public class FundHoldingsTickerDTO
{
    public required DateTime Date { get; set; }
    public required string Company { get; set; }
    public required string Ticker { get; set; }
    public required decimal MarketValueUsd { get; set; }
    public decimal WeightPercentage { get; set; }
    
    public override bool Equals(object? obj) => this.Equals(obj as FundHoldingsTickerDTO);

    public override int GetHashCode()
    {
        return HashCode.Combine(Date, Company, Ticker, MarketValueUsd, WeightPercentage);
    }

    public bool Equals(FundHoldingsTickerDTO? p)
    {
        if (p is null)
        {
            return false;
        }
        if (Object.ReferenceEquals(this, p))
        {
            return true;
        }
        if (this.GetType() != p.GetType())
        {
            return false;
        }
        
        return (Date == p.Date) &&
               (Company == p.Company) &&
               (Ticker == p.Ticker) &&
               (MarketValueUsd == p.MarketValueUsd) &&
               (WeightPercentage == p.WeightPercentage);
    }

};
