namespace DAL.DTO;


internal class TickerDataCsvRecord
{
     [CsvHelper.Configuration.Attributes.Name("date")]
     public DateTime Date { get; set; }
     [CsvHelper.Configuration.Attributes.Name("company")]
     public required string Company { get; set; }
     [CsvHelper.Configuration.Attributes.Name("ticker")]
     public required string Ticker { get; set; }
     [CsvHelper.Configuration.Attributes.Name("market value ($)")]
     public required string MarketValueUsd { get; set; }
     [CsvHelper.Configuration.Attributes.Name("weight (%)")]
     public required string WeightPercentage { get; set; }
}
