using FakeItEasy;
using NodaTime.Testing;
using NodaTime;
using arkk_analysis_tool;

namespace Tests;
public class CsvDownloaderTests
{   
    private readonly string _url;
    private readonly string _userAgent;
    private readonly string _path;
    private readonly string _outputPath;
    private readonly FakeClock _fakeClock;
    
    public CsvDownloaderTests()
    {
        _url = "https://ark-funds.com/wp-content/uploads/funds-etf-csv/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv";
        _userAgent = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.3";
        _path = "../../../TestData/ARK_INNOVATION_ETF_ARKK_HOLDINGS.csv";
        
        var fixedInstant = Instant.FromUtc(2024, 5, 29, 0, 0);
        _fakeClock = new FakeClock(fixedInstant);
        _outputPath = $"{fixedInstant.InZone(DateTimeZoneProviders.Tzdb.GetSystemDefault()):dd_MM_yyyy}_ARKK_HOLDINGS.csv";
    }

    private CsvDownloader SetupDownloader(HttpResponseMessage response)
    {
        var handler = A.Fake<HttpMessageHandler>();
        A.CallTo(handler)
            .WithReturnType<Task<HttpResponseMessage>>()
            .Where(call => call.Method.Name == "SendAsync")
            .Returns(Task.FromResult(response));

        return new CsvDownloader(handler, _userAgent, _fakeClock);
    }

    [Fact]
    public async Task TestDownloadCsv_CreateFile()
    {
        var response = new HttpResponseMessage
        {
            Content = new StringContent("Content is not important.")
        };
        var csvDownloader = SetupDownloader(response);
        
        await csvDownloader.DownloadCsv(_url);
        
        Assert.True(File.Exists(_outputPath));
    }

    [Fact]
    public async Task TestDownloadCsv_CompareResults()
    {   
        var expectedContent = await File.ReadAllTextAsync(_path);
        var response = new HttpResponseMessage
        {
            Content = new StringContent(expectedContent)
        };
        var csvDownloader = SetupDownloader(response);
        
        await csvDownloader.DownloadCsv(_url);
        var actualContent = await File.ReadAllTextAsync(_outputPath);
        
        Assert.Equal(CleanOutput(expectedContent), actualContent);
    }

    [Fact]
    public async Task TestDownloadCsv_CutFooter()
    {
        var expectedContent = await File.ReadAllTextAsync($"../../../CsvDownloaderTests.TestDownloadCsv_CutFooter.csv");
        var response = new HttpResponseMessage
        {
            Content = new StringContent(expectedContent)
        };
        var csvDownloader = SetupDownloader(response);
        
        await csvDownloader.DownloadCsv(_url);
        var actualContent = await File.ReadAllTextAsync(_outputPath);
        
        Assert.NotEqual(expectedContent, actualContent);
    }
    
    private string CleanOutput(string input)
    {
        // Replace non-breaking space (inserted by dotnet) with regular space
        input = input.Replace("\u202F", " ");
        input = input.Replace("\r\n", "\n");
        return input;
    }
}
