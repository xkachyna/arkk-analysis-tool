using DAL.DTO;
using TickerDateComparator;

namespace Tests;

public class DateDiffTests
{
    private List<FundHoldingsTickerDTO> _tickers;
    
    public DateDiffTests()
    {
        _tickers = new List<FundHoldingsTickerDTO>
        {
            new()
            {
                Date = new DateTime(2024, 3, 15),
                Company = "Tesla Inc.",
                Ticker = "TSLA",
                MarketValueUsd = 1000,
                WeightPercentage = (decimal)10.2,
            },
            new()
            {
                Date = new DateTime(2024, 3, 15),
                Company = "Coinbase Global Inc.",
                Ticker = "COIN",
                MarketValueUsd = 500,
                WeightPercentage = (decimal)5.1,
            },
            new()
            {
                Date = new DateTime(2024, 3, 17),
                Company = "Coinbase Global Inc.",
                Ticker = "COIN",
                MarketValueUsd = 550,
                WeightPercentage = (decimal)5.1,
            },
        };
    }
    
    private Dictionary<string, FundHoldingsTickerDTO> GetTickersForDate(DateTime date)
    {
        return _tickers.Where(t => t.Date == date).ToDictionary(t => t.Ticker);
    }
    
    [Fact]
    public void TestDiffSameDate()
    {
        TickerDateComparatorDiffCalculator comparatorDiff = new();
        DateTime date = new (2024, 3, 15);
        var firstDateTickers = GetTickersForDate(date);
        var secondDateTickers = GetTickersForDate(date);
        var diffDict = comparatorDiff.CalculateDifferenceBetweenDates(date, date, firstDateTickers, secondDateTickers);
        Assert.Equal(0, diffDict["TSLA"].ValueDiff);
        Assert.Equal(0, diffDict["TSLA"].WeightDiff);
        Assert.Equal(0, diffDict["TSLA"].PercentChange);
        Assert.Equal(0, diffDict["COIN"].ValueDiff);
        Assert.Equal(0, diffDict["COIN"].WeightDiff);
        Assert.Equal(0, diffDict["COIN"].PercentChange);
    }
    
    [Fact]
    public void TestDiffSecondBothDatesPresent()
    {
        TickerDateComparatorDiffCalculator comparatorDiff = new();
        DateTime date = new (2024, 3, 15);
        DateTime date2 = new(2024, 3, 17);
        
        var firstDateTickers = GetTickersForDate(date);
        var secondDateTickers = GetTickersForDate(date2);
        var diffDict = comparatorDiff.CalculateDifferenceBetweenDates(date, date2, firstDateTickers, secondDateTickers);
        
        Assert.Equal(50, diffDict["COIN"].ValueDiff);
        Assert.Equal((decimal)10.0, diffDict["COIN"].PercentChange);
    }

    [Fact]
    public void TestDiffSecondDateMissing()
    {
        TickerDateComparatorDiffCalculator comparatorDiff = new();
        DateTime date = new (2024, 3, 15);
        DateTime date2 = new(2024, 3, 17);
        
        var firstDateTickers = GetTickersForDate(date);
        var secondDateTickers = GetTickersForDate(date2);
        var diffDict = comparatorDiff.CalculateDifferenceBetweenDates(date, date2, firstDateTickers, secondDateTickers);
        
        Assert.Equal(-1000, diffDict["TSLA"].ValueDiff);
        Assert.Equal(-100, diffDict["TSLA"].PercentChange);
        Assert.Equal(-100, diffDict["TSLA"].WeightDiff);
    }
    
    [Fact]
    public void TestDiffFirstDateMissing()
    {
        TickerDateComparatorDiffCalculator comparatorDiff = new();
        DateTime date = new (2024, 3, 10);
        DateTime date2 = new(2024, 3, 17);
        
        var firstDateTickers = GetTickersForDate(date);
        var secondDateTickers = GetTickersForDate(date2);
        var diffDict = comparatorDiff.CalculateDifferenceBetweenDates(date, date2, firstDateTickers, secondDateTickers);
        
        Assert.Equal(550, diffDict["COIN"].ValueDiff);
        Assert.Equal(100, diffDict["COIN"].PercentChange);
        Assert.Equal(100, diffDict["COIN"].WeightDiff);
    }
}
