using FakeItEasy;
using MailDispatch;
using MailKit;
using MailKit.Net.Smtp;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;

namespace Tests;

public class MailDispatchTests
{
    [Fact]
    public void TestMailDispatch()
    {
        ConfigurationContext configurationContext = A.Fake<ConfigurationContext>();
        A.CallTo(() => configurationContext.GetMailSenderConfig()).Returns(new MailSenderConfig
        {
            SenderAddress = "test_sender_addr",
            SenderPassword = "test_sender_pass",
            SMTPHost = "test_host",
            SMTPPort = 12341235
        });
        A.CallTo(() => configurationContext.GetRecipientEmailAddress()).Returns("test_recipient");
        A.CallTo(() => configurationContext.GetTickerRepositoryPath()).Returns("../../../TestData/");
        
        var fakeSmtpClient = A.Fake<SmtpClient>();
        
        var serviceProviderBuilder = new ServiceProviderBuilder(configurationContext);
        serviceProviderBuilder.UseSmtpMailSender();
        serviceProviderBuilder.UseSmtpClient(fakeSmtpClient);
        var serviceProvider = serviceProviderBuilder.Build();
        var mailDispatch = serviceProvider.GetRequiredService<MailDispatch.MailDispatch>();
        mailDispatch.Run();
        
        A.CallTo(() => fakeSmtpClient.Send(A<MimeMessage>._, A<CancellationToken>._, A<ITransferProgress>._)
        ).MustHaveHappenedOnceExactly();
        
        A.CallTo(() => fakeSmtpClient.Connect(
            "test_host", 12341235, MailKit.Security.SecureSocketOptions.StartTls,
            A<System.Threading.CancellationToken>._)).MustHaveHappenedOnceExactly();
    }
}
