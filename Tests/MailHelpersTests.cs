﻿using MailDispatch;

namespace Tests;

public class MailHelpersTests
{
    [Fact]
    public void TestHelpersParseSMTPPort()
    {
        Assert.Throws<Exception>(() => Helpers.ParseSMTPPort("invalid"));
        Assert.Equal(25, Helpers.ParseSMTPPort("25"));
        Assert.Equal(1200, Helpers.ParseSMTPPort("1200"));
    }

    [Fact]
    public void TestHelpersIsValidEmail()
    {
        Assert.True(Helpers.IsValidEmail("thisisavalidmail@email.com"));
        Assert.False(Helpers.IsValidEmail("thisisnotavalidmail"));
        Assert.False(Helpers.IsValidEmail("thisisnotavalidmail@"));
        Assert.False(Helpers.IsValidEmail("thisisnotavalidmail@.com"));
    }

    [Fact]
    public void TestHelpersInputEmail()
    {
        Console.SetIn(new StringReader("validmail@email.com"));
        string result = Helpers.InputEmail("Input a valid email:");
        Assert.Equal("validmail@email.com", result);
    }
}