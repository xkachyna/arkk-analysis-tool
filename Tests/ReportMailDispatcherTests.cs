using System.Text.RegularExpressions;
using DAL.Repositories;
using TickerDateComparator;
using MailDispatch;
using ApprovalTests;
using ApprovalTests.Reporters;
namespace Tests;

public class ReportMailDispatcherTests
{
    private ReportMailDispatcher _reportMailDispatcherPlainText;
    private ReportMailDispatcher _reportMailDispatcherHtml;
    
    public ReportMailDispatcherTests()
    {
        MailSenderConfig config = new()
        {
            SenderAddress = "sender@gmail.com",
            SenderPassword = "testPassword",
            SMTPHost = "smtp.test",
            SMTPPort = 587
        };
        
        var mailSender = new DummyMailSender(config);
        IFundHoldingsTickerReadonlyRepository repository = new FundHoldingsTickerRepositoryCsv("../../../TestData/");
        TickerDateComparatorDiffCalculator comparatorDiff = new TickerDateComparatorDiffCalculator();
        
        IStockReportFormatter plainTextReportFormatter = new StockReportFormatterPlainText();
        _reportMailDispatcherPlainText = new ReportMailDispatcher(mailSender, repository, plainTextReportFormatter, comparatorDiff);
        
        IStockReportFormatter htmlReportFormatter = new StockReportFormatterHTMLText();
        _reportMailDispatcherHtml = new ReportMailDispatcher(mailSender, repository, htmlReportFormatter, comparatorDiff);
    }

    [Fact]
    public void TestDispatchReportForDates_Populated_PlainText()
    {
        using (StringWriter sw = new StringWriter())
        {
            Console.SetOut(sw);
            DateTime firstDate = new(2024, 3, 15);
            DateTime secondDate = new(2024, 3, 17);
            var recipientEmailAddress = "test@test.test";

            _reportMailDispatcherPlainText.DispatchReportForDates(firstDate, secondDate, recipientEmailAddress);
            var expected = File.ReadAllText("../../../ReportMailDispatcherTests.TestDispatchReportForDates_Populated_PlainText.approved.txt");
            var scrubbedOutput = CleanOutput(sw.ToString());
            Assert.Equal(CleanOutput(expected), scrubbedOutput);
        }
    }

    [Fact]
    public void TestDispatchReportForDates_Empty_PlainText()
    {
        using (StringWriter sw = new StringWriter())
        {
            Console.SetOut(sw);
            DateTime firstDate = new(2000, 1, 1);
            DateTime secondDate = new(2000, 1, 1);
            var recipientEmailAddress = "test@test.test";

            _reportMailDispatcherPlainText.DispatchReportForDates(firstDate, secondDate, recipientEmailAddress);
            var expected = File.ReadAllText("../../../ReportMailDispatcherTests.TestDispatchReportForDates_Empty_PlainText.approved.txt");
            var scrubbedOutput = CleanOutput(sw.ToString());
            Assert.Equal(CleanOutput(expected), scrubbedOutput);
            Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
        }
    }

    [Fact]
    public void TestDispatchReportForDates_Populated_HtmlText()
    {
        using (StringWriter sw = new StringWriter())
        {
            Console.SetOut(sw);
            DateTime firstDate = new(2024, 3, 15);
            DateTime secondDate = new(2024, 3, 17);
            var recipientEmailAddress = "test@test.test";

            _reportMailDispatcherHtml.DispatchReportForDates(firstDate, secondDate, recipientEmailAddress);
            var expected = File.ReadAllText("../../../ReportMailDispatcherTests.TestDispatchReportForDates_Populated_HtmlText.approved.txt");
            var scrubbedOutput = CleanOutput(sw.ToString());
            Assert.Equal(CleanOutput(expected), scrubbedOutput);
            Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
        }
    }

    [Fact]
    public void TestDispatchReportForDates_Empty_HtmlText()
    {
        using (StringWriter sw = new StringWriter())
        {
            Console.SetOut(sw);
            DateTime firstDate = new(2000, 1, 1);
            DateTime secondDate = new(2000, 1, 1);
            var recipientEmailAddress = "test@test.test";

            _reportMailDispatcherHtml.DispatchReportForDates(firstDate, secondDate, recipientEmailAddress);
            var expected = File.ReadAllText("../../../ReportMailDispatcherTests.TestDispatchReportForDates_Empty_HtmlText.approved.txt");
            var scrubbedOutput = CleanOutput(sw.ToString());
            Assert.Equal(CleanOutput(expected), scrubbedOutput);
            Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
        }
    }
    
    private string CleanOutput(string input)
    {
        input = Regex.Replace(input, @"Subject: Stock analysis for \d{4}-\d{1,2}-\d{1,2}", "Subject: Stock analysis for yyyy-mm-dd");
        // Replace non-breaking space (inserted by dotnet) with regular space
        input = input.Replace("\u202F", " ");
        input = input.Replace("\r\n", "\n");
        return input;
    }
}
