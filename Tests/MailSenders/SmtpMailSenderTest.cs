using MailDispatch;
using FakeItEasy;
using MailKit;
using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace Tests.MailSenders;

public class SmtpMailSenderTest
{
    [Fact]
    public void TestSmtpMailSender_SendStockReport_Plaintext_Sent()
    {
        var fakeSmtpClient = A.Fake<SmtpClient>();
        
        var smtpMailSender = new SmtpMailSender(new MailSenderConfig
        {
            SenderAddress = "test_sender_addr",
            SenderPassword = "test_sender_pass",
            SMTPHost = "test_host",
            SMTPPort = 9234234
        }, fakeSmtpClient);
        
        smtpMailSender.SendStockReport("test", new StockReport
        {
            Body = "test",
            BodyFormat = StockReportBodyFormat.PlainText,
            SenderName = "test_sender",
            Subject = "Test subject"
        });
        
        A.CallTo(() => fakeSmtpClient.Connect(
            "test_host", 9234234, SecureSocketOptions.StartTls,
            A<CancellationToken>._)).MustHaveHappenedOnceExactly();
        
        A.CallTo(() => fakeSmtpClient.Send(
            A<MimeMessage>.That.Matches(
                m => m.To.ToString() == "test" && m.Subject == "Test subject" &&
                m.Body.ContentType.MimeType.ToString() == "text/plain" &&
                m.TextBody.ToString() == "test"),
                A<CancellationToken>._, A<ITransferProgress>._)).MustHaveHappenedOnceExactly();
        
        A.CallTo(() => fakeSmtpClient.Disconnect(true, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }
    
    [Fact]
    public void TestSmtpMailSender_SendStockReport_Html_Sent()
    {
        var fakeSmtpClient = A.Fake<SmtpClient>();
        
        var smtpMailSender = new SmtpMailSender(new MailSenderConfig
        {
            SenderAddress = "test_sender_addr",
            SenderPassword = "test_sender_pass",
            SMTPHost = "test_host",
            SMTPPort = 9234234
        }, fakeSmtpClient);
        
        smtpMailSender.SendStockReport("test", new StockReport
        {
            Body = "<p>test</p>",
            BodyFormat = StockReportBodyFormat.Html,
            SenderName = "test_sender",
            Subject = "Test subject"
        });
        
        A.CallTo(() => fakeSmtpClient.Connect(
            "test_host", 9234234, SecureSocketOptions.StartTls,
            A<CancellationToken>._)).MustHaveHappenedOnceExactly();
        
        A.CallTo(() => fakeSmtpClient.Send(
            A<MimeMessage>.That.Matches(
                m => m.To.ToString() == "test" && m.Subject == "Test subject" &&
                m.Body.ContentType.MimeType.ToString() == "text/html" &&
                m.HtmlBody.ToString() == "<p>test</p>"),
                A<CancellationToken>._, A<ITransferProgress>._)).MustHaveHappenedOnceExactly();
        
        A.CallTo(() => fakeSmtpClient.Disconnect(true, A<CancellationToken>._)).MustHaveHappenedOnceExactly();
    }
}
