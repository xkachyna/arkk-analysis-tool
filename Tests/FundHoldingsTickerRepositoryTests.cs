using DAL.DTO;
using DAL.Repositories;

namespace Tests;

public class FundHoldingsTickerRepositoryTests
{
    private IFundHoldingsTickerReadonlyRepository _repository;
    
    public FundHoldingsTickerRepositoryTests()
    {
        _repository = new FundHoldingsTickerRepositoryCsv("../../../TestData");
    }

    [Fact]
    public void TestGetFundHoldingsTickersForDate()
    {
        var date = new DateTime(2024, 3, 15);
        var tickers = _repository.GetFundHoldingsTickersForDate(date);
        Assert.True(tickers.Count > 0);
    }
    [Fact]
    public void TestGetFundHoldingsTickerForDateInvalid()
    {
        var funds = _repository.GetFundHoldingsTickersForDate(new DateTime(3000, 1, 1));
        Assert.Empty(funds);
    }

    [Fact]
    public void TestGetFundHoldingsTickerHistory()
    {
        var funds = _repository.GetFundHoldingsTickerHistory("TWOU");
        var tickerRecord = new FundHoldingsTickerDTO
        {
            Date = new DateTime(2024, 3, 15),
            Company = "2U INC",
            Ticker = "TWOU",
            MarketValueUsd = (decimal)2041909.15,
            WeightPercentage = (decimal)0.03
        };
        Assert.Single(funds);
        Assert.Equal(funds.First(), tickerRecord);
    }

    [Fact]
    public void TestGetFundHoldingsTickerHistoryInvalidTicker()
    {
        var funds = _repository.GetFundHoldingsTickerHistory("INVALID_TICKER");
        Assert.Empty(funds);

    }

    [Fact]
    public void TestLoadCsvDirectory()
    {
        var funds = _repository.GetFundHoldingsTickersForDate(new DateTime(2024, 3, 15));
        Assert.Equal(38, funds.Count);
        funds = _repository.GetFundHoldingsTickersForDate(new DateTime(2024, 3, 17));
        Assert.Equal(2, funds.Count);
        
    }
}
