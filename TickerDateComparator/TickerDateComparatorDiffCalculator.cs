using DAL.DTO;

namespace TickerDateComparator;

/// <summary>
/// Class for creating the difference between ticker records
/// </summary>
public class TickerDateComparatorDiffCalculator
{
    /// <summary>
    /// Gets a dict containing diff objects with diff information between two specific dates 
    /// </summary>
    /// <param name="firstDate">Starting date to compare against</param>
    /// <param name="secondDate">Second date to compare the first day with</param>
    /// <param name="tickersForFirstDate">Ticker values for first date</param>
    /// <param name="tickersForSecondDate">Ticker values for second date</param>
    /// <returns>Dictionary of TickerDiff objects containing the diff information</returns>
    public Dictionary<string, TickerDiff> CalculateDifferenceBetweenDates(
        DateTime firstDate, DateTime secondDate,
        Dictionary<string, FundHoldingsTickerDTO> tickersForFirstDate,
        Dictionary<string, FundHoldingsTickerDTO> tickersForSecondDate)
    {
        var difference = new Dictionary<string, TickerDiff>();

        foreach (var ticker in tickersForFirstDate.Keys)
        {
            var firstDateTicker = tickersForFirstDate[ticker];
            TickerDiff tickerDiff;
            
            if (tickersForSecondDate.TryGetValue(ticker, out var secondDateTicker))
            {
                tickerDiff = CreateTickerDiff(firstDateTicker, secondDateTicker);
            }
            else
            {
                tickerDiff = CreateTickerDiffAllSold(firstDate, secondDate, firstDateTicker);
            }
            
            difference.Add(ticker, tickerDiff);
        }
        
        // Handle tickers that are in the end date but not in the start date
        foreach (var ticker in tickersForSecondDate.Keys)
        {
            if (tickersForFirstDate.ContainsKey(ticker))
                continue;
            
            difference.Add(ticker, CreateTickerDiffNewTicker(firstDate, secondDate, tickersForSecondDate[ticker]));
        }

        return difference;
    }

    private static TickerDiff CreateTickerDiffNewTicker(DateTime firstDate, DateTime secondDate,
        FundHoldingsTickerDTO secondDateTicker)
    {
        return new TickerDiff
        {
            ValueDiff = secondDateTicker.MarketValueUsd,
            PercentChange = 100,
            WeightDiff = 100,
            FirstDate = firstDate,
            SecondDate = secondDate
        };
    }

    private TickerDiff CreateTickerDiff(FundHoldingsTickerDTO firstDateTicker, FundHoldingsTickerDTO secondDateTicker)
    {
        decimal valueDiff = secondDateTicker.MarketValueUsd - firstDateTicker.MarketValueUsd;
        decimal percentChange = CalculatePercentChange(firstDateTicker.MarketValueUsd, secondDateTicker.MarketValueUsd);

        var tickerDiff = new TickerDiff
        {
            ValueDiff = valueDiff,
            PercentChange = percentChange,
            WeightDiff = secondDateTicker.WeightPercentage - firstDateTicker.WeightPercentage,
            FirstDate = firstDateTicker.Date,
            SecondDate = secondDateTicker.Date
        };
        return tickerDiff;
    }

    /// <summary>
    /// Case where ticker is in day 1 but not day 2 meaning all shares were sold
    /// </summary>
    /// <param name="firstDate"></param>
    /// <param name="secondDate"></param>
    /// <param name="firstDateTicker"></param>
    /// <returns></returns>
    private static TickerDiff CreateTickerDiffAllSold(DateTime firstDate, DateTime secondDate,
        FundHoldingsTickerDTO firstDateTicker)
    {
        return new TickerDiff
        {
            ValueDiff = -firstDateTicker.MarketValueUsd,
            PercentChange = -100,
            WeightDiff = -100,
            FirstDate = firstDate,
            SecondDate = secondDate
        };
    }

    private decimal CalculatePercentChange(decimal startValue, decimal endValue)
    {
        if (startValue == 0)
        {
            if (endValue == 0)
            {
                return 0;
            }

            return 100;
        }

        return ((endValue - startValue) / startValue) * 100;
    }
}
