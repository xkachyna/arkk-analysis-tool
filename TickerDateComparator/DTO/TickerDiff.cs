﻿namespace  TickerDateComparator;

public class TickerDiff
{
    public DateTime FirstDate { get; set; }
    public DateTime SecondDate { get; set; }
    public decimal ValueDiff { get; set; }
    public decimal PercentChange { get; set; }
    public decimal WeightDiff { get; set; }
}