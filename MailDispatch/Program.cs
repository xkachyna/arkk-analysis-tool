using Microsoft.Extensions.DependencyInjection;

namespace MailDispatch;
static class Program
{
    static void Main()
    {
        var configurationContext = new ConfigurationContext();
        var serviceProviderBuilder = new ServiceProviderBuilder(configurationContext);
        serviceProviderBuilder.UseDummyMailSender();
        
        var serviceProvider = serviceProviderBuilder.Build();
        var mailDispatch = serviceProvider.GetRequiredService<MailDispatch>();
        
        mailDispatch.Run();
    }
}
