using System.Globalization;
using TickerDateComparator;
using Tests;

namespace MailDispatch;

public class StockReportFormatterHTMLText: IStockReportFormatter
{
    public StockReport CreateReport(DateTime date, DateTime date2, Dictionary<string, TickerDiff> diffDict)
    {
        return new StockReport
        {
            SenderName = "Ark Invest Tool",
            Subject = $"Stock analysis for {DateTimeFormatter.FormatDateTime(DateTime.Today.Date)}",
            Body = CreateBody(date, date2, diffDict),
            BodyFormat = StockReportBodyFormat.Html
        };
    }
    private static string CreateBody(DateTime date, DateTime date2, Dictionary<string, TickerDiff> diffDict)
    {
        var htmlContent = InsertHTMLStyle(date, date2);

        htmlContent = InsetTableContent(diffDict, htmlContent);
        
        htmlContent += @"</tbody>
                </table>
                </div>
            </body>
            </html>";

        return htmlContent;
    }

    private static string InsertHTMLStyle(DateTime date, DateTime date2)
    {
        string htmlContent = @"
            <!DOCTYPE html>
            <html lang=""en"">
            <head>
                <meta charset=""UTF-8"">
                <meta name=""viewport"" content=""width=device-width, initial-scale=1.0"">
                <title>Ticker Information</title>
                <style>
                    body {
                        font-family: Arial, sans-serif;
                        background-color: #f0f0f0;
                        margin: auto;
                        padding: 20px;
                    }
                    .container {
                        text-align: center;
                    }
                    h1 {
                        font-weight: bold;
                        margin: 10px 0;
                    }
                    table {
                        width: 80%;
                        margin: 20px auto;
                        border-collapse: collapse;
                    }
                    th, td {
                        padding: 10px;
                        border: 1px solid #ddd;
                        font-size: 16px;
                    }
                    th {
                        background-color: #f2f2f2;
                        font-weight: bold;
                    }
                    .ticker-info {
                        background-color: #fff;
                        border-radius: 5px;
                        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
                        padding: 20px;
                    }
                    h1 {
                        font-size: 24px;
                        margin-top: 0;
                    }
                    .label {
                        font-weight: bold;
                    }
                    #ticker {
                        color: #007bff; /* blue color */
                    }
                    #red {
                        color: #dc3545; /* red color */
                    }
                    #black {
                        color: #000000; /* black color */
                    }
                    #green {
                        color: #28a745; /* green color */
                    }
                </style>
            </head>
            <body>
                <div class=""container"">
                    <h1>Ticker Analysis</h1>
                    <h2>Dates: " + DateTimeFormatter.FormatDate(date) + " - " + DateTimeFormatter.FormatDate(date2) + @"</h2>
                    <table>
                        <thead>
                            <tr>
                                <th>Ticker</th>
                                <th>$ Difference</th>
                                <th>% Difference</th>
                                <th>Weight Difference</th>
                            </tr>
                        </thead>
                        <tbody>";
        return htmlContent;
    }

    private static string InsetTableContent(Dictionary<string, TickerDiff> diffDict, string htmlContent)
    {
        var ci = new CultureInfo("en-US");
        foreach (var kvp in diffDict)
        {
            string ticker = kvp.Key;
            TickerDiff tickerDiff = kvp.Value;

            htmlContent += $@"
            <tr>
            <td id=""ticker""><a href=""https://finance.yahoo.com/quote/{ticker.Split(' ').First()}"">{ticker}</a></td>
            <td id=""{RedOrGreenTextColor(tickerDiff.ValueDiff)}"">{tickerDiff.ValueDiff.ToString("F2", ci)}</td>
            <td id=""{RedOrGreenTextColor(tickerDiff.PercentChange)}"">{tickerDiff.PercentChange.ToString("F2", ci)}</td>
            <td id=""{RedOrGreenTextColor(tickerDiff.WeightDiff)}"">{tickerDiff.WeightDiff.ToString("F2", ci)}</td>
            </tr>";
        }

        return htmlContent;
    }

    private static string RedOrGreenTextColor(decimal value)
    {
        if (value == 0)
        {
            return "black";
        }
        return value > 0 ? "green" : "red";
    }

        

}
