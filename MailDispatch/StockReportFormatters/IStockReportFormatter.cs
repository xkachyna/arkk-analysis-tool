using TickerDateComparator;

namespace MailDispatch;

/// <summary>
/// Tool for creating formatted stock reports ready for sending
/// </summary>
public interface IStockReportFormatter
{
    /// <summary>
    /// Creates a new stock report comparing tickers between two dates
    /// </summary>
    /// <param name="date"></param>
    /// <param name="date2"></param>
    /// <param name="diffDict"></param>
    /// <returns></returns>
    StockReport CreateReport(DateTime date, DateTime date2, Dictionary<string, TickerDiff> diffDict);
}
