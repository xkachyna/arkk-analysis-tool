using System.Globalization;

namespace Tests;

public static class DateTimeFormatter
{
    public static string FormatDate(DateTime date)
    {
        return date.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture);
    }
    
    public static string FormatDateTime(DateTime date)
    {
        return date.ToString("yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
    }
}
