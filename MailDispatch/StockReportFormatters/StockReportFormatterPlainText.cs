using System.Globalization;
using TickerDateComparator;
using Tests;

namespace MailDispatch;

public class StockReportFormatterPlainText: IStockReportFormatter
{
    public StockReport CreateReport(DateTime date, DateTime date2, Dictionary<string, TickerDiff> diffDict)
    {
        return new StockReport
        {
            SenderName = "Ark Invest Tool",
            Subject = $"Stock analysis for {DateTimeFormatter.FormatDateTime(DateTime.Today.Date)}",
            Body = CreateBody(date, date2, diffDict),
            BodyFormat = StockReportBodyFormat.PlainText
        };
    }
    
    private static string CreateBody(DateTime date, DateTime date2, Dictionary<string, TickerDiff> diffDict)
    {
        var ci = new CultureInfo("en-US");
        string text = $"Updated Analysis between days {DateTimeFormatter.FormatDate(date)} and {DateTimeFormatter.FormatDate(date2)}\n";
        foreach (var kvp in diffDict)
        {
            string ticker = kvp.Key;
            TickerDiff tickerDiff = kvp.Value;

            text += $"Ticker: {ticker}\n";
            text += $"Value Difference: {tickerDiff.ValueDiff.ToString("F2", ci)}\n";
            text += $"Percent Change: {tickerDiff.PercentChange.ToString("F2", ci)}\n";
            text += $"Weight Difference: {tickerDiff.WeightDiff.ToString("F2", ci)}\n";
            text += "\n"; 
        }

        return text;
    }
}
