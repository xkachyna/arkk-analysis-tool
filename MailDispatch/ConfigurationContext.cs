using System.Collections.Specialized;
using System.Configuration;

namespace MailDispatch;

public class ConfigurationContext
{
    private readonly NameValueCollection settings;
    
    public ConfigurationContext()
    {
        try
        {
            settings = (NameValueCollection)ConfigurationManager.GetSection("appSettings");
        }
        catch (ConfigurationErrorsException ex)
        {
            throw new ConfigurationErrorsException($"Error reading app settings: {ex.Message}");
        }
    }
    
    public virtual string GetRecipientEmailAddress()
    {
        try
        {
            return settings["RecipientEmailAddress"]!;
        } 
        catch (Exception)
        {
            throw new Exception("RecipientEmailAddress is not set in the configuration file.");
        }
    }
    
    public virtual string GetTickerRepositoryPath()
    {
        try
        {
            return settings["TickerRepositoryPath"]!;
        } 
        catch (Exception)
        {
            throw new Exception("TickerRepositoryPath is not set in the configuration file.");
        }
    }
    
    public virtual MailSenderConfig GetMailSenderConfig()
    {
        foreach (var key in new[] {"SenderAddress", "SenderPassword", "SMTPHost", "SMTPPort"})
        {
            if (string.IsNullOrWhiteSpace(settings[key]))
            {
                throw new Exception($"{key} is not set in the configuration file.");
            }
        }
            
        MailSenderConfig config = new()
        {
            SenderAddress = settings["SenderAddress"]!,
            SenderPassword = settings["SenderPassword"]!,
            SMTPHost = settings["SMTPHost"]!,
            SMTPPort = Helpers.ParseSMTPPort(settings["SMTPPort"]!),
        };

        return config;
    }
}