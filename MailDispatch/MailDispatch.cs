using System.Configuration;
using Microsoft.Extensions.Logging;

namespace MailDispatch;

public class MailDispatch
{
    ReportMailDispatcher reportMailDispatcher { get; }
    ConfigurationContext configurationContext { get; }

    private ILogger logger { get; }

    public MailDispatch(ReportMailDispatcher reportMailDispatcher, ConfigurationContext configurationContext,ILogger<MailDispatch> logger)
    {
        this.reportMailDispatcher = reportMailDispatcher;
        this.configurationContext = configurationContext;
        this.logger = logger;
        
    }
    
    public void Run()
    {
        var recipientEmailAddress = configurationContext.GetRecipientEmailAddress();
        if (string.IsNullOrWhiteSpace(recipientEmailAddress))
        {
            recipientEmailAddress = Helpers.InputEmail("Please type recipient email:");
        }
        
        DateTime firstDate = new(2024, 3, 14);
        DateTime secondDate = new(2024, 3, 15);
        
        try
        {
            reportMailDispatcher.DispatchReportForDates(firstDate, secondDate, recipientEmailAddress);
            logger.LogInformation("Dispatched the report successfully");

        }
        catch(Exception ex)
        {
            logger.LogError(ex, "Error dispatching the report.");
            throw;
        }
    }
}