namespace MailDispatch;

/// <summary>
/// Used to simulate sending an email for testing purposes.
/// No actual email is sent, but the details of the email are printed to the console.
/// </summary>
public class DummyMailSender: IMailSender
{
    protected MailSenderConfig _config;
    
    public DummyMailSender(MailSenderConfig config)
    {
        _config = config;
    }
    
    public void SendStockReport(string receiver, StockReport report)
    {
        Console.WriteLine("=== Dummy Mail Sender ===");
        Console.WriteLine($"Sent mail from: '{report.SenderName} {_config.SenderAddress}' to '{receiver}'");
        Console.WriteLine($"Using SMTP host: {_config.SMTPHost} and port: {_config.SMTPPort}");
        Console.WriteLine($"Subject: {report.Subject}");
        Console.WriteLine($"Body:\n{report.Body}");
    }
}
