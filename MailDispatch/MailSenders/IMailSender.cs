namespace MailDispatch;

public interface IMailSender
{
    void SendStockReport(string receiver, StockReport email);
}
