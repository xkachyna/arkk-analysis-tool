using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;

namespace MailDispatch;

/// <summary>
/// Mail sender class that sends emails using the SMTP protocol.
/// </summary>
public class SmtpMailSender: IMailSender
{
    private readonly SmtpClient _client;
    protected MailSenderConfig _config;
    
    public SmtpMailSender(MailSenderConfig config, SmtpClient client)
    {
        _config = config;
        _client = client;
    }
    
    public void SendStockReport(string receiver, StockReport report)
    {
        var message = CreateMimeMessageFromReport(receiver, report);

        _client.Connect(_config.SMTPHost, _config.SMTPPort, SecureSocketOptions.StartTls);
        _client.Authenticate(_config.SenderAddress, _config.SenderPassword);
        _client.Send(message);
        _client.Disconnect(true);
    }

    private MimeMessage CreateMimeMessageFromReport(string receiver, StockReport report)
    {
        var message = new MimeMessage();
        message.From.Add(new MailboxAddress(report.SenderName, _config.SenderAddress));
        message.To.Add(new MailboxAddress("", receiver));
        message.Subject = report.Subject;
        message.Body = new TextPart(report.BodyFormat.GetBodyFormatString()) { Text = report.Body };
        return message;
    }
}
