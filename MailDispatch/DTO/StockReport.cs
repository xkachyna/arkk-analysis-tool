namespace MailDispatch;

public class StockReport
{
    public required string SenderName { get; set; }
    public required string Subject { get; set; }
    public required string Body { get; set; }
    public StockReportBodyFormat BodyFormat { get; set; } = StockReportBodyFormat.Html;
}

public enum StockReportBodyFormat
{
    PlainText,
    Html
}

static class StockReportBodyFormatMethods
{
    public static string GetBodyFormatString(this StockReportBodyFormat format)
    {
        switch (format)
        {
            case StockReportBodyFormat.PlainText:
                return "plain";
            case StockReportBodyFormat.Html:
                return "html";
            default:
                throw new ArgumentOutOfRangeException(nameof(format), format, null);
        }
    }
}
