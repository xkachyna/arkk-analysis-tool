namespace MailDispatch;

public class MailSenderConfig
{
    public required string SenderAddress { get; set; }
    public required string SenderPassword { get; set; }
    public required string SMTPHost { get; set; }
    public required int SMTPPort { get; set; }
}