using DAL.Repositories;
using MailKit.Net.Smtp;
using Microsoft.Extensions.DependencyInjection;
using TickerDateComparator;
using Microsoft.Extensions.Logging;

namespace MailDispatch;

// IoC container
public class ServiceProviderBuilder
{
    private readonly ServiceCollection serviceCollection;
    
    public ServiceProviderBuilder(ConfigurationContext configurationContext)
    {
        serviceCollection = new ServiceCollection();
        serviceCollection.AddSingleton(configurationContext);
        serviceCollection.AddSingleton(configurationContext.GetMailSenderConfig());
        var tickerRepositoryPath = configurationContext.GetTickerRepositoryPath();
        UseDummyMailSender();
        serviceCollection.AddTransient<IFundHoldingsTickerReadonlyRepository>(
            s => new FundHoldingsTickerRepositoryCsv(tickerRepositoryPath));
        serviceCollection.AddTransient<IStockReportFormatter, StockReportFormatterHTMLText>();
        serviceCollection.AddTransient<TickerDateComparatorDiffCalculator>();
        serviceCollection.AddTransient<ReportMailDispatcher>();
        serviceCollection.AddTransient<MailDispatch>();
        serviceCollection.AddLogging(configure => configure.AddConsole());
        UseSmtpClient(new SmtpClient());
    }
    
    public void UseSmtpClient(SmtpClient smtpClient)
    {
        serviceCollection.AddSingleton(smtpClient);
    }
    
    public void UseDummyMailSender()
    {
        serviceCollection.AddTransient<IMailSender, DummyMailSender>();
    }
    
    public void UseSmtpMailSender()
    {
        serviceCollection.AddTransient<IMailSender, SmtpMailSender>();
    }
    
    public IServiceProvider Build()
    {
        return serviceCollection.BuildServiceProvider();
    }
}