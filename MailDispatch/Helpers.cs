﻿using System.Text.RegularExpressions;

namespace MailDispatch
{
    public static class Helpers
    {
        public static int ParseSMTPPort(string value)
        {
            if (!int.TryParse(value, out var port))
            {
                throw new Exception("SMTPPort value is not valid.");
            }

            return port;
        }

        public static string InputEmail(string prompt)
        {
            while (true)
            {
                Console.WriteLine(prompt);
                string? emailAddress = Console.ReadLine();

                if (emailAddress is not null && IsValidEmail(emailAddress))
                {
                    return emailAddress;
                }

                Console.WriteLine($"Invalid email address '{emailAddress}'. Please try again...");
            }
        }

        public static bool IsValidEmail(string email)
        {
            string pattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$";

            return Regex.IsMatch(email, pattern);

        }
    }
}