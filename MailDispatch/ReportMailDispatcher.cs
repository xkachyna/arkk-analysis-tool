using DAL.Repositories;
using TickerDateComparator;

namespace MailDispatch;

public class ReportMailDispatcher
{
    private IMailSender MailSender;
    private IFundHoldingsTickerReadonlyRepository Repository;
    private IStockReportFormatter ReportFormatter;
    private TickerDateComparatorDiffCalculator _tickerDateComparatorDiffCalculator;
    
    public ReportMailDispatcher(
        IMailSender mailSender, IFundHoldingsTickerReadonlyRepository repository,
        IStockReportFormatter reportFormatter, TickerDateComparatorDiffCalculator tickerDateComparatorDiffCalculator)
    {
        MailSender = mailSender;
        Repository = repository;
        ReportFormatter = reportFormatter;
        _tickerDateComparatorDiffCalculator = tickerDateComparatorDiffCalculator;
    }
    
    public void DispatchReportForDates(DateTime firstDate, DateTime secondDate, string recipientEmailAddress)
    {
        var tickersForFirstDate = Repository.GetFundHoldingsTickersForDate(firstDate);
        var tickersForSecondDate = Repository.GetFundHoldingsTickersForDate(secondDate);
        var diffDict = _tickerDateComparatorDiffCalculator.CalculateDifferenceBetweenDates(firstDate, secondDate, tickersForFirstDate, tickersForSecondDate);
        var report = ReportFormatter.CreateReport(firstDate, secondDate, diffDict);
        
        MailSender.SendStockReport(recipientEmailAddress, report);
    }
}
