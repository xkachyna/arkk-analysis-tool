# arkk-analysis-tool
Updated 2024-04-28.

Tool for downloading and analyzing ARKK holdings ticker data.

## Requirements
- .NET 8.0

## Usage
### Download tool
- Entrypoint: `Downloader/Program.cs`
- Configuration: `Downloader/app.config`

Tool for downloading and saving CSV files with ARKK holdings ticker data.
Running the script will download CSV data for the current date and save them
to a predefined directory.

### Analysis tool
- Entrypoint: `MailDispatch/Program.cs`
- Configuration: `MailDispatch/app.config`

Tool for analyzing ARKK holdings ticker data. The tool will read CSV files from
a predefined directory and calculate the following metrics for each ticker:
- Absolute change (in USD)
- Percentage change
- Weight change

The tool will send an email with the calculated metrics to a predefined email address
using the SMTP server specified in the configuration file. The email will contain
a formatted report in either HTML or plain text format.

## Project structure
- `DAL` - Data access layer for reading CSV files
- `DateDiff` - Utility for calculating ticker differences
- `Downloader` - Tool for downloading ARKK holdings ticker data
- `MailDispatch` - Tool for analyzing ARKK holdings ticker data and sending reports
- `Tests` - Unit tests for the project
