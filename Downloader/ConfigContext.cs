using System.Configuration;

namespace arkk_analysis_tool;

public class ConfigContext
{
    public static (string url, string userAgent) GetValuesFromConfig()
    {
        string? url;
        string? userAgent;
        
        url = ConfigurationManager.AppSettings["DownloadUrl"];
        userAgent = ConfigurationManager.AppSettings["UserAgent"];
  
        if (string.IsNullOrWhiteSpace(userAgent))
        {
            throw new ArgumentNullException(userAgent,  
                "User agent is not set in the configuration file.");
        }
        
        if ( string.IsNullOrWhiteSpace(url))
        {
            throw new ArgumentNullException(url,  
                "Url is not set in the configuration file.");
        }

        return (url, userAgent);
    }
}