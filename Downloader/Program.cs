﻿namespace arkk_analysis_tool;

class Program
{
    static async Task Main()
    {
        var (url, userAgent) = ConfigContext.GetValuesFromConfig();
        var csvDownloader = new CsvDownloader(userAgent);
        
        try
        {
            await csvDownloader.DownloadCsv(url);
            Console.WriteLine("CSV file downloaded successfully.");
        }
        catch (Exception ex)
        {
            throw new Exception($"Error downloading the CSV file: {ex.Message}");
        }
    }
}