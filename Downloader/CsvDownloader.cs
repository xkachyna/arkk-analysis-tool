using NodaTime;

namespace arkk_analysis_tool;

public class CsvDownloader
{
    private readonly string _outputPath;
    private readonly HttpClient _client;

    public CsvDownloader(string userAgent)
    {
        _client = new HttpClient();
        _client.DefaultRequestHeaders.UserAgent.ParseAdd(userAgent);
        _outputPath = $"{DateTime.Now:dd_MM_yyyy}_ARKK_HOLDINGS.csv";
    }
    
    public CsvDownloader(HttpMessageHandler handler, string userAgent, IClock clock)
    {
        _client = new HttpClient(handler);
        _client.DefaultRequestHeaders.UserAgent.ParseAdd(userAgent);
        _outputPath = $"{clock.GetCurrentInstant():dd_MM_yyyy}_ARKK_HOLDINGS.csv";
    }

    public async Task DownloadCsv(string url)
    {
        using HttpResponseMessage response = await _client.GetAsync(url);
        await using Stream contentStream = await response.Content.ReadAsStreamAsync();
        using StreamReader reader = new StreamReader(contentStream);
        await using StreamWriter writer = new StreamWriter(_outputPath);
        while (await reader.ReadLineAsync() is { } line)
        {
            if (line.StartsWith("\"Investors "))
                break;
            await writer.WriteLineAsync(line);
        }
    }
}
